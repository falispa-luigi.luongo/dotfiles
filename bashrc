# Funzione per ottenere lo stato del repository Git corrente
git_prompt() {
	local git_status="$(git status 2>/dev/null)"
	if [[ ! -z "$git_status" ]]; then
		# Ottiene il nome del branch
		local on_branch="$(git rev-parse --abbrev-ref HEAD)"
		# Verifica se il repository ha modifiche non committate
		local uncommitted_changes=""
		if [[ $(git diff --shortstat 2>/dev/null | tail -n1) != "" ]]; then
			uncommitted_changes="*"
		fi
		# Stampa il nome del branch e lo stato
		echo " ($on_branch$uncommitted_changes)"
	fi
}

gc() { git commit -m "$1"; }
ga() { git add $1; }
gp() { git pull $1 $2; }

# SSH Function
ssh_with_key_selection() {
	# Definisci la directory delle chiavi SSH
	KEY_DIR="$HOME/.ssh"
	echo "Seleziona una chiave SSH da utilizzare:"

	# List certificate files
	select KEY_FILE in $(ls $KEY_DIR/*.pem) "NO  Certificate"; do
		if [[ "$KEY_FILE" == "Nessuna key" ]]; then
			KEY_OPTION=""
			echo "Nessuna chiave SSH selezionata."
			break
		else
			KEY_OPTION="-i $KEY_FILE"
			echo "Chiave SSH selezionata: $KEY_FILE"
			break
		fi
	done

	# Request user input
	read -p "Inserisci l'username: " SSH_USER
	read -p "Inserisci l'IP: " SSH_IP

	SSH_COMMAND="ssh $KEY_OPTION $SSH_USER@$SSH_IP"
	eval $SSH_COMMAND
}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

export PATH=$PATH:/opt/homebrew/bin:$HOME/Sviluppo/scripts

# Variabile prompt PS1
export PS1="\[\e[32m\]\u:\W\$(git_prompt)\[\e[0m\]$ > "

# PYENV
#export PYENV_ROOT="$HOME/.pyenv"
#[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
#eval "$(pyenv init -)"

## ALIAS
alias ls='ls --color=auto'
alias tmux='tmux -f ~/.config/tmux.conf'
alias sshk='ssh_with_key_selection'
alias dev='cd /Users/luigi/Sviluppo/dev'

# GIT ALIAS
alias push='git push'
alias status='git status'
